{
  docker run -i --name tests -v ./ -w /usr/src/app node:alpine sh -c "npm install husky@2.4 && npm install && npm run lint && npm test" &&
  LC_TIME=en_US date > index.html &&
  echo '<pre>' >> index.html &&
  docker run --rm grycap/cowsay /usr/games/cowsay "Tests were passed" >> index.html &&
  echo '</pre>' >> index.html &&
  docker rm tests
} || {
  echo '<pre>' > index.html &&
  docker logs tests 2>&1 | tee -a index.html &&
  echo '</pre>' >> index.html
  docker rm tests
}

echo 'FROM nginx:latest' > Dockerfile
echo 'COPY ./index.html /usr/share/nginx/html' >> Dockerfile

docker login registry.gitlab.com -u "gitlab+deploy-token-tests" -p $DEPLOY_TOKEN
docker pull registry.gitlab.com/viktorov04/array_tests_cases/logs
docker tag registry.gitlab.com/viktorov04/array_tests_cases/logs registry.gitlab.com/viktorov04/array_tests_cases/logs:previous
docker push registry.gitlab.com/viktorov04/array_tests_cases/logs:previous
docker rmi registry.gitlab.com/viktorov04/array_tests_cases/logs:previous
docker build -t registry.gitlab.com/viktorov04/array_tests_cases/logs . 
docker push registry.gitlab.com/viktorov04/array_tests_cases/logs


